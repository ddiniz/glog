package glog

import (
	"fmt"
	"log"
	"os"
	"time"
)

type LogLevelEnum int

const (
	RESET                   = "\033[0m"
	DARK_RED                = "\033[31m"
	DARK_GREEN              = "\033[32m"
	DARK_YELLOW             = "\033[33m"
	DARK_BLUE               = "\033[34m"
	DARK_PURPLE             = "\033[35m"
	DARK_CYAN               = "\033[36m"
	LIGHT_GRAY              = "\033[37m"
	DARK_GRAY               = "\033[90m"
	RED                     = "\033[91m"
	GREEN                   = "\033[92m"
	YELLOW                  = "\033[93m"
	BLUE                    = "\033[94m"
	PURPLE                  = "\033[95m"
	CYAN                    = "\033[96m"
	WHITE                   = "\033[97m"
	BLACK_BACKGROUND        = "\033[40m"
	DARK_RED_BACKGROUND     = "\033[41m"
	DARK_GREEN_BACKGROUND   = "\033[42m"
	DARK_YELLOW_BACKGROUND  = "\033[43m"
	DARK_BLUE_BACKGROUND    = "\033[44m"
	DARK_MAGENTA_BACKGROUND = "\033[45m"
	DARK_CYAN_BACKGROUND    = "\033[46m"
	LIGHT_GRAY_BACKGROUND   = "\033[47m"
	DARK_GRAY_BACKGROUND    = "\033[100m"
	RED_BACKGROUND          = "\033[101m"
	GREEN_BACKGROUND        = "\033[102m"
	ORANGE_BACKGROUND       = "\033[103m"
	BLUE_BACKGROUND         = "\033[104m"
	MAGENTA_BACKGROUND      = "\033[105m"
	CYAN_BACKGROUND         = "\033[106m"
	WHITE_BACKGROUND        = "\033[107m"
)

const (
	TRACE_TAG   = "[TRACE]   "
	DEBUG_TAG   = "[DEBUG]   "
	INFO_TAG    = "[INFO]    "
	WARN_TAG    = "[WARN]    "
	ERROR_TAG   = "[ERROR]   "
	FATAL_TAG   = "[FATAL]   "
	PANIC_TAG   = "[PANIC]   "
	SUCCESS_TAG = "[SUCCESS] "
)

const (
	TRACE LogLevelEnum = iota
	DEBUG
	INFO
	WARN
	ERROR
)

var options GlogOptions
var outputFile *os.File

type GlogOptions struct {
	Level                    LogLevelEnum
	PrintToTerminal          bool
	OutputFilePath           string
	PrintTimestampOnTerminal bool
	TimestampFormat          string
}

func NewLogger(level LogLevelEnum) error {
	setPrintToTerminal(true)
	setLoggingLevel(level)
	return nil
}

func NewLoggerWithOptions(_options GlogOptions) error {
	setLoggingLevel(_options.Level)
	setPrintToTerminal(_options.PrintToTerminal)
	setPrintTimestampOnTerminal(_options.PrintTimestampOnTerminal)
	if len(_options.TimestampFormat) > 0 {
		setTimestampFormat(_options.TimestampFormat)
	} else {
		setTimestampFormat("2006-01-02 15:04:05")
	}

	if len(_options.OutputFilePath) > 0 {
		var err error
		outputFile, err = openFile(_options.OutputFilePath)
		log.SetOutput(outputFile)
		if err != nil {
			return err
		}
	}
	return nil
}

func Close() {
	if outputFile != nil {
		outputFile.Close()
	}
}

func openFile(logFilePath string) (*os.File, error) {
	file, err := os.Create(logFilePath)
	if err != nil {
		return nil, fmt.Errorf("error creating log file at %s : %s", logFilePath, err.Error())
	}
	return file, nil
}

func print(msg string, color string) {
	if options.PrintToTerminal {
		if options.PrintTimestampOnTerminal {
			timeStr := fmt.Sprintf("[%s] ", time.Now().Format(options.TimestampFormat))
			fmt.Printf(color + timeStr + msg + RESET + "\n")
		} else {
			fmt.Printf(color + msg + RESET + "\n")
		}
	}
	if outputFile != nil {
		log.Print(msg)
	}
}

func printFatal(msg string, color string) {
	if options.PrintToTerminal {
		if options.PrintTimestampOnTerminal {
			timeStr := fmt.Sprintf("[%s] ", time.Now().Format(options.TimestampFormat))
			fmt.Printf(color + timeStr + msg + RESET + "\n")
		} else {
			fmt.Printf(color + msg + RESET + "\n")
		}
		os.Exit(1)
	}
	if outputFile != nil {
		log.Fatal(msg)
	}
}

func printPanic(msg string, color string) {
	if options.PrintToTerminal {
		res := ""
		if options.PrintTimestampOnTerminal {
			timeStr := fmt.Sprintf("[%s] ", time.Now().Format(options.TimestampFormat))
			res = fmt.Sprintf(color + timeStr + msg + RESET + "\n")
		} else {
			res = fmt.Sprintf(color + msg + RESET + "\n")
		}
		defer panic(res)
	}
	if outputFile != nil {
		log.Panic(msg)
	}
}

func Trace(msg string, args ...any) string {
	errMsg := TRACE_TAG + fmtErr(msg, args...)
	if options.Level <= TRACE {
		print(errMsg, RESET)
	}
	return errMsg
}
func Debug(msg string, args ...any) string {
	errMsg := DEBUG_TAG + fmtErr(msg, args...)
	if options.Level <= DEBUG {
		print(errMsg, PURPLE)
	}
	return errMsg
}
func Info(msg string, args ...any) string {
	errMsg := INFO_TAG + fmtErr(msg, args...)
	if options.Level <= INFO {
		print(errMsg, BLUE)
	}
	return errMsg
}
func Warn(msg string, args ...any) string {
	errMsg := WARN_TAG + fmtErr(msg, args...)
	if options.Level <= WARN {
		print(errMsg, YELLOW)
	}
	return errMsg
}
func Error(msg string, args ...any) string {
	errMsg := ERROR_TAG + fmtErr(msg, args...)
	if options.Level <= ERROR {
		print(errMsg, DARK_RED)
	}
	return errMsg
}
func Fatal(msg string, args ...any) string {
	defer Close()
	errMsg := FATAL_TAG + fmtErr(msg, args...)
	printFatal(errMsg, DARK_RED_BACKGROUND)
	return errMsg
}
func Panic(msg string, args ...any) string {
	defer Close()
	errMsg := PANIC_TAG + fmtErr(msg, args...)
	printPanic(errMsg, RED)
	return errMsg
}
func Success(msg string, args ...any) string {
	errMsg := SUCCESS_TAG + fmtErr(msg, args...)
	print(errMsg, GREEN)
	return errMsg
}
func fmtErr(msg string, args ...any) string {
	return fmt.Sprintf(msg, args...)
}
