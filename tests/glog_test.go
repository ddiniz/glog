package tests

import (
	"testing"

	"gitlab.com/ddiniz/glog"
)

func TestMain(test *testing.T) {
	glog.NewLoggerWithOptions(glog.GlogOptions{
		Level:                    glog.TRACE,
		PrintToTerminal:          true,
		OutputFilePath:           "./log.log",
		PrintTimestampOnTerminal: true,
	})
	glog.Trace("Trace")
	glog.Debug("Debug")
	glog.Info("Info")
	glog.Warn("Warn")
	glog.Error("Error")
	glog.Success("Success")
	glog.Panic("Panic")
}

func TestPanic(test *testing.T) {
	glog.NewLoggerWithOptions(glog.GlogOptions{
		Level:                    glog.TRACE,
		PrintToTerminal:          true,
		OutputFilePath:           "./log.log",
		PrintTimestampOnTerminal: true,
	})
	glog.Panic("Panic")
}

func TestFatal(test *testing.T) {
	glog.NewLoggerWithOptions(glog.GlogOptions{
		Level:                    glog.TRACE,
		PrintToTerminal:          true,
		OutputFilePath:           "./log.log",
		PrintTimestampOnTerminal: true,
	})
	glog.Fatal("Fatal")
}

func TestColors(test *testing.T) {
	println(glog.RESET, "RESET", glog.RESET)
	println(glog.DARK_RED, "DARK_RED", glog.RESET)
	println(glog.DARK_GREEN, "DARK_GREEN", glog.RESET)
	println(glog.DARK_YELLOW, "DARK_YELLOW", glog.RESET)
	println(glog.DARK_BLUE, "DARK_BLUE", glog.RESET)
	println(glog.DARK_PURPLE, "DARK_PURPLE", glog.RESET)
	println(glog.DARK_CYAN, "DARK_CYAN", glog.RESET)
	println(glog.LIGHT_GRAY, "LIGHT_GRAY", glog.RESET)
	println(glog.DARK_GRAY, "DARK_GRAY", glog.RESET)
	println(glog.RED, "RED", glog.RESET)
	println(glog.GREEN, "GREEN", glog.RESET)
	println(glog.YELLOW, "YELLOW", glog.RESET)
	println(glog.BLUE, "BLUE", glog.RESET)
	println(glog.PURPLE, "PURPLE", glog.RESET)
	println(glog.CYAN, "CYAN", glog.RESET)
	println(glog.WHITE, "WHITE", glog.RESET)
	println(glog.BLACK_BACKGROUND, "BLACK_BACKGROUND", glog.RESET)
	println(glog.DARK_RED_BACKGROUND, "DARK_RED_BACKGROUND", glog.RESET)
	println(glog.DARK_GREEN_BACKGROUND, "DARK_GREEN_BACKGROUND", glog.RESET)
	println(glog.DARK_YELLOW_BACKGROUND, "DARK_YELLOW_BACKGROUND", glog.RESET)
	println(glog.DARK_BLUE_BACKGROUND, "DARK_BLUE_BACKGROUND", glog.RESET)
	println(glog.DARK_MAGENTA_BACKGROUND, "DARK_MAGENTA_BACKGROUN", glog.RESET)
	println(glog.DARK_CYAN_BACKGROUND, "DARK_CYAN_BACKGROUND", glog.RESET)
	println(glog.LIGHT_GRAY_BACKGROUND, "LIGHT_GRAY_BACKGROUND", glog.RESET)
	println(glog.DARK_GRAY_BACKGROUND, "DARK_GRAY_BACKGROUND", glog.RESET)
	println(glog.RED_BACKGROUND, "RED_BACKGROUND", glog.RESET)
	println(glog.GREEN_BACKGROUND, "GREEN_BACKGROUND", glog.RESET)
	println(glog.ORANGE_BACKGROUND, "ORANGE_BACKGROUND", glog.RESET)
	println(glog.BLUE_BACKGROUND, "BLUE_BACKGROUND", glog.RESET)
	println(glog.MAGENTA_BACKGROUND, "MAGENTA_BACKGROUND", glog.RESET)
	println(glog.CYAN_BACKGROUND, "CYAN_BACKGROUND", glog.RESET)
	println(glog.WHITE_BACKGROUND, "WHITE_BACKGROUND", glog.RESET)
}
